<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:36
 */

namespace BinaryStudioAcademy\Game;


use BinaryStudioAcademy\Game\Harbors\AbstractHarbor;
use BinaryStudioAcademy\Game\Harbors\PiratesHarbor;
use BinaryStudioAcademy\Game\Interfaces\Map;
use BinaryStudioAcademy\Game\Ship\Ship;
use BinaryStudioAcademyTests\Game\Messages;

class Location implements Map
{
    private $harbor;
    private $player;
    private $ship;

    public function __construct()
    {
        $this->harbor = new PiratesHarbor();
        $this->player = new Player();
    }

    public function getHarbor(): AbstractHarbor
    {
        return $this->harbor;
    }

    public function changeHarbor(AbstractHarbor $harbor)
    {
        $this->harbor = $harbor;
    }

    public function getShip(): Ship
    {
        return $this->ship;
    }

    public function getPlayer(): Player
    {
        return $this->player;
    }

    public function currentHarborNumber()
    {
        return $this->getHarbor()->getNumber();
    }

    public function changeHarborByDirection($direction)
    {
        $harborNumber = $this->harbor->isNearestHarbor($direction);
        if ($harborNumber) {
            $harborObj = "\\BinaryStudioAcademy\\Game\\Harbors\\" . $this->harbor->getHarborByNumber($harborNumber);
            $this->harbor = new $harborObj;
            if ($harborNumber == 1) {
                unset($this->ship);

            } else {
                $shipObj = "\\BinaryStudioAcademy\\Game\\Ship\\" . $this->harbor->getShipByHarborNumber($harborNumber);
                $this->ship = new $shipObj;
            }
        }
        return $harborNumber;
    }

    public function fire()
    {
        $battle = new BattleProcess($this->player, $this->getShip());
        if (!$this->player->getIsDie() && !$this->getShip()->getIsDie()) {
            $battle->battleProcess();
            if ($this->getShip()->getIsDie()) {
                return Messages::win($this->getShip()->getName());
            }
            if ($this->player->getIsDie()) {
                unset($this->ship);
                $this->harbor = new PiratesHarbor();
                $this->player->setIsDie(false);
                return Messages::die();
            }
            return Messages::fire($this->getShip()->getName(), $this->player->getDamage(), $this->getShip()->getHealth(), $this->getShip()->getDamage(), $this->player->getHealth());
        }
        return Messages::errors('pirate_harbor_fire');
    }

    public function aboard()
    {
        $hold = $this->player->getHold();
        if (count($hold) < 3) {
            $shipType = $this->getShip()->getType();
            $this->player->setHold(array_merge($hold, $this->getShip()->getHold()));
            return $shipType;
        }
        return false;
    }

}