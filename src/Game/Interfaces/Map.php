<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:35
 */

namespace BinaryStudioAcademy\Game\Interfaces;


use BinaryStudioAcademy\Game\Harbors\AbstractHarbor;
use BinaryStudioAcademy\Game\Player;
use BinaryStudioAcademy\Game\Ship\Ship;

interface Map
{
    public function getHarbor(): AbstractHarbor;

    public function changeHarbor(AbstractHarbor $harbor);

    public function currentHarborNumber();

    public function changeHarborByDirection($direction);

    public function getShip(): Ship;

    public function getPlayer(): Player;

    public function aboard();

    public function fire();

}