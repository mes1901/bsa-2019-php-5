<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 15:43
 */

namespace BinaryStudioAcademy\Game\Ship;


class Schooner extends Ship
{
    public function __construct()
    {
        $this->type = 'schooner';
        $this->name = 'Royal Patrool Schooner';
        $this->armour = 4;
        $this->health = 50;
        $this->luck = 4;
        $this->strength = 4;
        $this->hold = ['gold'];
        $this->damage = 0;
        $this->isDie = false;
    }

}