<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 15:43
 */

namespace BinaryStudioAcademy\Game\Ship;


class Battle extends Ship
{
    public function __construct()
    {
        $this->type = 'battle';
        $this->name = 'Royal BattleProcess Ship';
        $this->armour = 8;
        $this->health = 80;
        $this->luck = 7;
        $this->strength = 8;
        $this->hold = ['rom'];
        $this->damage = 0;
        $this->isDie = false;
    }
}