<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 13:03
 */

namespace BinaryStudioAcademy\Game\Ship;


use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\Random;

abstract class Ship
{
    protected $name;
    protected $type;
    protected $strength;
    protected $armour;
    protected $luck;
    protected $health;
    protected $damage;
    protected $isDie;

    protected $hold;

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getStrength()
    {
        return $this->strength;
    }

    public function getArmour()
    {
        return $this->armour;
    }

    public function getLuck()
    {
        return $this->luck;
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function getHold()
    {
        return $this->hold;
    }

    public function getDamage()
    {
        return $this->damage;
    }

    public function getIsDie()
    {
        return $this->isDie;
    }

    public function setStrength($strength)
    {
        $this->strength = $strength;
    }

    public function setArmour($armour)
    {
        $this->armour = $armour;
    }

    public function setLuck($luck)
    {
        $this->luck = $luck;
    }

    public function setHealth($health)
    {
        $this->health = $health;
    }

    public function setHold($hold)
    {
        $this->hold = $hold;
    }

    public function setDamage($damage)
    {
        $this->damage = $damage;
    }

    public function setIsDie($isDie)
    {
        $this->isDie = $isDie;
    }

    public function makeDamage()
    {
        $this->setDamage((new Math())->damage($this->getStrength(), $this->getArmour()));
    }

    public function applyDamage($damage)
    {
        $this->setHealth($this->getHealth() - $damage);
    }

    public function isHit(): bool
    {
        return (new Math())->luck(new Random(), $this->getLuck());
    }

}