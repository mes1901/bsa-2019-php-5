<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 15:43
 */

namespace BinaryStudioAcademy\Game\Ship;


class Royal extends Ship
{
    public function __construct()
    {
        $this->type = 'royal';
        $this->name = 'HMS Royal Sovereign';
        $this->armour = 10;
        $this->health = 100;
        $this->luck = 10;
        $this->strength = 10;
        $this->hold = ['gold','gold','rom'];
        $this->damage = 0;
        $this->isDie = false;
    }

}