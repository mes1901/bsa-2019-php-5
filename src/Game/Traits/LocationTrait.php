<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:14
 */

namespace BinaryStudioAcademy\Game\Traits;


trait LocationTrait
{
    public function getAvailableDirections(): string
    {
        return implode(", ", array_keys(self::AVAILABLE_DIRECTIONS));
    }

    public function isNearestHarbor($direction): int
    {
        if (array_key_exists($direction, self::AVAILABLE_DIRECTIONS)) {
            return self::AVAILABLE_DIRECTIONS[$direction];
        }
        return false;
    }
}