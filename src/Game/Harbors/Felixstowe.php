<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:12
 */

namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\Ship\Battle;
use BinaryStudioAcademy\Game\Traits\LocationTrait;

class Felixstowe extends AbstractHarbor
{
    use LocationTrait;

    const AVAILABLE_DIRECTIONS = ['north' => 5, 'west' => 2, 'east' => 8];

    public function __construct()
    {
        $this->number = 7;
        $this->name = 'Felixstowe';
        $this->ship = new Battle();
    }

    public function getShip()
    {
        return $this->ship;
    }

}