<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:12
 */

namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\Ship\Royal;
use BinaryStudioAcademy\Game\Traits\LocationTrait;

class LondonDocks extends AbstractHarbor
{
    use LocationTrait;
    const AVAILABLE_DIRECTIONS = ['north' => 6, 'west' => 7];

    public function __construct()
    {
        $this->number = 8;
        $this->name = 'London Docks';
        $this->ship = new Royal();
    }

}