<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:09
 */

namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\Traits\LocationTrait;

class PiratesHarbor extends AbstractHarbor
{
    use LocationTrait;
    const AVAILABLE_DIRECTIONS = ['north' => 4, 'west' => 3, 'south' => 2];

    public function __construct()
    {
        $this->number = 1;
        $this->name = 'Pirates harbor';
    }

}