<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:10
 */

namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\Ship\Schooner;
use BinaryStudioAcademy\Game\Traits\LocationTrait;

class Fishguard extends AbstractHarbor
{
    use LocationTrait;
    const AVAILABLE_DIRECTIONS = ['north' => 4, 'south' => 2, 'east' => 1];

    public function __construct()
    {
        $this->number = 3;
        $this->name = 'Fishguard';
        $this->ship = new Schooner();
    }

}