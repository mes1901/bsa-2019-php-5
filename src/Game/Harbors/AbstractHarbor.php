<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 16:29
 */

namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademyTests\Game\Messages;

abstract class AbstractHarbor
{
    protected $number;
    protected $name;
    protected $ship;

    public function getName()
    {
        return $this->name;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function getShip()
    {
        return $this->ship;
    }

    public function getHarborByNumber($number)
    {
        $harbor = Messages::HARBORS[$number]['harbor'];
        return str_replace(' ', '', $harbor);
    }

    public function getShipByHarborNumber($number)
    {
        $ship = Messages::HARBORS[$number]['ship'];
        return ucfirst($ship);
    }


}