<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:11
 */

namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\Ship\Battle;
use BinaryStudioAcademy\Game\Traits\LocationTrait;

class Grays extends AbstractHarbor
{
    use LocationTrait;
    const AVAILABLE_DIRECTIONS = ['west' => 5, 'south' => 8];

    public function __construct()
    {
        $this->number = 6;
        $this->name = 'Grays';
        $this->ship = new Battle();
    }

}