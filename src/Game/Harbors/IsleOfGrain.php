<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:11
 */

namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\Ship\Schooner;
use BinaryStudioAcademy\Game\Traits\LocationTrait;

class IsleOfGrain extends AbstractHarbor
{
    use LocationTrait;
    const AVAILABLE_DIRECTIONS = ['west' => 4, 'south' => 7, 'east' => 6];

    public function __construct()
    {
        $this->number = 5;
        $this->name = 'Isle of Grain';
        $this->ship = new Schooner();
    }
}