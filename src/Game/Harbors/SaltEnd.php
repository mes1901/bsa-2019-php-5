<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 05.07.2019
 * Time: 17:11
 */

namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\Ship\Schooner;
use BinaryStudioAcademy\Game\Traits\LocationTrait;

class SaltEnd extends AbstractHarbor
{
    use LocationTrait;
    const AVAILABLE_DIRECTIONS = ['west' => 3, 'south' => 1, 'east' => 5];

    public function __construct()
    {
        $this->number = 4;
        $this->name = 'Salt End';
        $this->ship = new Schooner();
    }

}