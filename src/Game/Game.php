<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class Game
{
    private $random;
    private $app;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->app = new GameServiceProvider(new Location(), new Player());
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln('Adventure has begun. Wish you good luck!');
        while (true) {
            $writer->write("Command: ");
            $this->run($reader, $writer);
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $input = trim($reader->read());
        $this->app->executeCommand($input);
        $writer->writeln($this->app->getCommandMessage());
    }
}
