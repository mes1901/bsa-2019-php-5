<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 08.07.2019
 * Time: 13:23
 */

namespace BinaryStudioAcademy\Game;


use BinaryStudioAcademy\Game\Commands\Aboard;
use BinaryStudioAcademy\Game\Commands\Buy;
use BinaryStudioAcademy\Game\Commands\Drink;
use BinaryStudioAcademy\Game\Commands\Fire;
use BinaryStudioAcademy\Game\Commands\Help;
use BinaryStudioAcademy\Game\Commands\Move;
use BinaryStudioAcademy\Game\Commands\Stats;
use BinaryStudioAcademy\Game\Commands\Unknown;
use BinaryStudioAcademy\Game\Commands\Whereami;
use BinaryStudioAcademy\Game\Interfaces\Map;

class GameServiceProvider
{
    public $map;
    public $player;
    public $command;

    public function __construct(Map $map, Player $player)
    {
        $this->map = $map;
        $this->player = $player;
    }

    public function executeCommand($input)
    {
        $params = explode(" ", $input);
        $command = array_shift($params);

        switch ($command) {
            case 'aboard' :
                $this->command = new Aboard($this->map, $this->player);
                break;

            case 'drink' :
                $this->command = new Drink($this->map, $this->player);
                break;

            case 'fire' :
                $this->command = new Fire($this->map, $this->player);
                break;

            case 'help' :
                $this->command = new Help($this->map, $this->player);
                break;

            case 'stats' :
                $this->command = new Stats($this->map, $this->player);
                break;

            case 'whereami' :
                $this->command = new Whereami($this->map, $this->player);
                break;

            case 'buy' :
                $this->command = new Buy($this->map, $this->player, array_shift($params));
                break;

            case 'set-sail' :
                $this->command = new Move($this->map, $this->player, array_shift($params));
                break;

            case 'exit' :
                exit();
                break;

            default :
                $this->command = new Unknown();
                break;
        }
    }

    public function getCommandMessage()
    {
        return $this->command->getMessage();
    }

}