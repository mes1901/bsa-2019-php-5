<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 08.07.2019
 * Time: 10:25
 */

namespace BinaryStudioAcademy\Game;


use BinaryStudioAcademy\Game\Ship\Ship;
use BinaryStudioAcademyTests\Game\Messages;

class BattleProcess
{
    protected $player;
    protected $enemy;

    public function __construct(Player $player, Ship $enemy)
    {
        $this->player = $player;
        $this->enemy = $enemy;
    }

    public function battleProcess()
    {
        if ($this->player->isHit()) {
            $this->player->makeDamage();
            $this->enemy->applyDamage($this->player->getDamage());
            if ($this->enemy->getHealth() <= 0) {
                $this->enemy->setIsDie(true);
                if ($this->enemy->getName() == 'HMS Royal Sovereign') {
                    echo Messages::finalWin();
                    exit();
                }
            }
        } else {
            $this->player->setDamage(0);
        }
        if (!$this->enemy->getIsDie() && $this->enemy->isHit()) {
            $this->enemy->makeDamage();
            $this->player->applyDamage($this->enemy->getDamage());
            if ($this->player->getHealth() <= 0) {
                $this->player->setIsDie(true);
                if ($this->player->loose()) {
                    echo "Wasted!";
                    exit();
                }
            }
        } else {
            $this->enemy->setDamage(0);
        }
    }

}