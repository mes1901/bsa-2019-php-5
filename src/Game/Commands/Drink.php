<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 16:55
 */

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;

class Drink extends AbstractCommands
{
    public function getMessage()
    {
        if ($this->map->getPlayer()->drink()) {
            return Messages::drink($this->map->getPlayer()->getHealth());
        }
        return "There is no rum in your hold!";
    }

}