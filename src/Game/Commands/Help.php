<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 16:30
 */

namespace BinaryStudioAcademy\Game\Commands;


use BinaryStudioAcademyTests\Game\Messages;

class Help extends AbstractCommands
{
    public function getMessage()
    {
        return Messages::help();
    }

}