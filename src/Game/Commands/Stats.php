<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 16:46
 */

namespace BinaryStudioAcademy\Game\Commands;


use BinaryStudioAcademyTests\Game\Messages;

class Stats extends AbstractCommands
{
    public function getMessage()
    {
        $data = [
            'strength' => $this->map->getPlayer()->getStrength(),
            'armour' => $this->map->getPlayer()->getArmour(),
            'luck' => $this->map->getPlayer()->getLuck(),
            'health' => $this->map->getPlayer()->getHealth(),
            'hold' => $this->map->getPlayer()->getHoldStr()
        ];
        return Messages::stats($data);
    }
}