<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 16:39
 */

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;

class Aboard extends AbstractCommands
{
    public function getMessage()
    {
        if ($this->map->getHarbor()->getNumber() != 1) {
            if ($this->map->getShip()->getIsDie()) {
                if ($ship = $this->map->aboard()) {
                    switch ($ship) {
                        case 'schooner':
                            return Messages::aboardSchooner();
                        case 'battle':
                            return Messages::aboardBattleShip();
                    }
                }
                return 'You have reached max items count of you hold!';
            }
            return Messages::errors('aboard_live_ship');
        }
        return Messages::errors('pirate_harbor_aboard');
    }

}