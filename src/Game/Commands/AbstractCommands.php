<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 16:18
 */

namespace BinaryStudioAcademy\Game\Commands;
use BinaryStudioAcademy\Game\Interfaces\Map;
use BinaryStudioAcademy\Game\Player;

abstract class AbstractCommands
{
    protected $map;
    protected $player;

    public function __construct(Map $map, Player $player)
    {
        $this->map = $map;
        $this->player = $player;
    }

    public abstract function getMessage();

}