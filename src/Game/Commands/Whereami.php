<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 16:44
 */

namespace BinaryStudioAcademy\Game\Commands;


use BinaryStudioAcademyTests\Game\Messages;

class Whereami extends AbstractCommands
{
    public function getMessage()
    {
        return Messages::whereAmI($this->map->currentHarborNumber());
    }

}