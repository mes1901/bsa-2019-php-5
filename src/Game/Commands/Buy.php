<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 16:55
 */

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Interfaces\Map;
use BinaryStudioAcademy\Game\Player;
use BinaryStudioAcademyTests\Game\Messages;

class Buy extends AbstractCommands
{
    private $item;

    public function __construct(Map $map, Player $player, $item)
    {
        parent::__construct($map, $player);
        $this->item = $item;
    }

    public function getMessage()
    {
        if ($this->map->currentHarborNumber() == 1) {
            if ($this->map->getPlayer()->buy($this->item)) {
                switch ($this->item) {
                    case 'strength':
                        return Messages::buy($this->item, $this->map->getPlayer()->getStrength());
                    case 'armour':
                        return Messages::buy($this->item, $this->map->getPlayer()->getArmour());
                    case 'luck':
                        return Messages::buy($this->item, $this->map->getPlayer()->getLuck());
                    case 'rum':
                        return Messages::buyRum($this->map->getPlayer()->rumCount());
                }
            }
            return "You have no gold!";
        }
        return "Move to the Pirates Harbor to buy!";
    }

}