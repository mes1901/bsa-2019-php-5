<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 16:38
 */

namespace BinaryStudioAcademy\Game\Commands;


use BinaryStudioAcademyTests\Game\Messages;

class Fire extends AbstractCommands
{
    public function getMessage()
    {
        if ($this->map->currentHarborNumber() != 1) {
            return $this->map->fire();
        }
        return Messages::errors('pirate_harbor_fire');
    }
}