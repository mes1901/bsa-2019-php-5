<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 08.07.2019
 * Time: 13:38
 */

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;

class Unknown
{
    public function getMessage()
    {
        return Messages::errors('unknown_command');
    }
}