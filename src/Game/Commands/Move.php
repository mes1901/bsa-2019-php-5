<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 16:26
 */

namespace BinaryStudioAcademy\Game\Commands;


use BinaryStudioAcademy\Game\Interfaces\Map;
use BinaryStudioAcademy\Game\Player;
use BinaryStudioAcademyTests\Game\Messages;

class Move extends AbstractCommands
{
    private $direction;

    public function __construct(Map $map, Player $player, $direction)
    {
        parent::__construct($map, $player);
        $this->direction = $direction;
    }

    public function getMessage()
    {
        $number = $this->map->changeHarborByDirection($this->direction);
        if ($number) {
            if ($number == 1) {
                if ($this->map->getPlayer()->refreshHealth()) {
                    return Messages::piratesHarbor();
                }
                return Messages::piratesHarborWithGoodHealth();
            }
            return Messages::harbor($this->map->currentHarborNumber());
        }
        return Messages::errors('incorrect_direction');
    }
}