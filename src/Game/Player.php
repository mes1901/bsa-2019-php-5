<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 06.07.2019
 * Time: 18:19
 */

namespace BinaryStudioAcademy\Game;


use BinaryStudioAcademy\Game\Helpers\Stats;
use BinaryStudioAcademy\Game\Ship\Ship;

class Player extends Ship
{
    public function __construct()
    {
        $this->type = 'pirate';
        $this->name = 'player';
        $this->armour = 4;
        $this->health = 60;
        $this->luck = 4;
        $this->strength = 4;
        $this->hold = [];
        $this->damage = 0;
        $this->isDie = false;
    }

    public function armorIncrease()
    {
        if ($this->armour < Stats::MAX_ARMOUR) {
            $this->armour++;
        }
        return "You have max armor stat!";
    }

    public function strengthIncrease()
    {
        if ($this->strength < Stats::MAX_STRENGTH) {
            $this->strength++;
        }
        return "You have max strength stat!";
    }

    public function luckIncrease()
    {
        if ($this->luck < Stats::MAX_LUCK) {
            $this->luck++;
        }
        return "You have max luck stat!";
    }

    public function healthIncrease()
    {
        if (($this->health += 30) > Stats::MAX_HEALTH) {
            $this->health = Stats::MAX_HEALTH;
        }
    }

    public function goldDecrease()
    {
        $hold_arr = $this->hold;
        unset($hold_arr[array_search('gold', $hold_arr)]);
        $this->setHold($hold_arr);
    }

    public function rumCount()
    {
        $count = 0;
        foreach ($this->hold as $rum) {
            if ($rum == 'rum') ++$count;
        }
        return $count;
    }

    public function buy($item)
    {
        if (in_array('gold', $this->hold)) {
            switch ($item) {
                case 'armour':
                    $this->armorIncrease();
                    $this->goldDecrease();
                    break;
                case 'strength':
                    $this->strengthIncrease();
                    $this->goldDecrease();
                    break;
                case 'luck':
                    $this->luckIncrease();
                    $this->goldDecrease();
                    break;
                case 'rum':
                    $this->goldDecrease();
                    array_push($this->hold, 'rum');
                    break;
            }
            return true;
        }
        return false;
    }

    public function drink()
    {
        $hold_arr = $this->getHold();
        if (in_array('rum', $hold_arr)) {
            unset($hold_arr[array_search('rum', $hold_arr)]);
            $this->setHold($hold_arr);
            $this->healthIncrease();
            return true;
        }
        return false;
    }

    public function getHoldStr()
    {
        return implode(', ', $this->getHold());
    }

    public function refreshHealth()
    {
        if ($this->health < 60) {
            $this->health = 60;
            return true;
        }
        return false;
    }

    public function loose()
    {
        if ($this->luck > 1 && $this->strength > 1 && $this->armour > 1) {
            $this->hold = [];
            $this->luck--;
            $this->strength--;
            $this->armour--;
            $this->health = 60;
            return false;
        }
        return true;
    }
}